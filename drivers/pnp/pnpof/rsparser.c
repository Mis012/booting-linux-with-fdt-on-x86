// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * pnpof -- PnP OF driver
 *
 * Copyright (c) 2021 Michael Srba <Michael.Srba@seznam.cz>
 * based on the pnpacpi driver
 */
#include <linux/kernel.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/pci.h>
#include <linux/pnp.h>
#include <linux/slab.h>
#include "../base.h"
#include "pnpof.h"

/*
 * Allocated Resources
 */

static void pnpof_add_irqresource(struct pnp_dev *dev, struct resource *r)
{
	pcibios_penalize_isa_irq(r->start, 1);

	pnp_add_resource(dev, r);
}

int pnpof_parse_allocated_resource(struct pnp_dev *dev)
{
	struct device_node *node = dev->data;
	struct resource res;
	int i;

	pnp_dbg(&dev->dev, "parse allocated resources\n");

	pnp_init_resources(dev);

	printk("::: parsing PNP resources from OF:\n");

	i = 0;
	while (of_address_to_resource(node, i, &res) == 0) {
		printk("::: adding address PNP resource: start: >%llx<\n", res.start);
		pnp_add_resource(dev, &res);
		i++;
	}

	i = 0;
	while (of_irq_to_resource(node, i, &res) >= 0) {
		pnpof_add_irqresource(dev, &res);
		i++;
	}

	return 0;
}

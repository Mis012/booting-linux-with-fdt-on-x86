/* SPDX-License-Identifier: GPL-2.0 */
#ifndef OF_PNP_H
#define OF_PNP_H

#include <linux/acpi.h>
#include <linux/pnp.h>

int pnpof_parse_allocated_resource(struct pnp_dev *);
#endif

// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * pnpof -- PnP OF driver
 *
 * Copyright (c) 2021 Michael Srba <Michael.Srba@seznam.cz>
 * based on the pnpacpi driver
 */

#include <linux/export.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/pnp.h>
#include <linux/slab.h>
#include <linux/mod_devicetable.h>

#include "../base.h"
#include "pnpof.h"

static int num;

/*
 * Compatible Device IDs
 */
#define TEST_HEX(c) \
	if (!(('0' <= (c) && (c) <= '9') || ('a' <= (c) && (c) <= 'f') || ('A' <= (c) && (c) <= 'F'))) \
		return 0
#define TEST_ALPHA(c) \
	if (!('A' <= (c) && (c) <= 'Z')) \
		return 0
static int __init ispnpidof(const char *id) // TODO make this less ugly if at all possible
{
	int i;

	printk("::: entered ispnpidof...\n");

	if(strncmp(id, "pnp", strlen("pnp"))) { // the compatible has to start with pnp per bindings
		printk("::: %d\n", __LINE__);
		return 0;
	}

	id += strlen("pnp"); // will make this a bit cleaner

	if(strlen(id) < 5) { // the proper format should look like "VND,<at least one hex digit>"
		printk("::: %d\n", __LINE__);
		return 0;
	}

	// test if the PNP Vendor ID looks legit

	TEST_ALPHA(id[0]);
	TEST_ALPHA(id[1]);
	TEST_ALPHA(id[2]);

	if(id[3] != ',') { // with PNP Vendor ID being three letters, the next char must be ','
		printk("::: %d\n", __LINE__);
		return 0;
	}

	i = 4; // the first digit of the hex number

	while(i < strlen(id)) {
		TEST_HEX(id[i]);
		i++;
	}

	printk("::: ispnpidof works...\n");

	return 1;
}

static void pnpof_id_of_to_canonic(char *dest, char *id)
{
	char vendor_id[3];
	id += strlen("pnp");

	strncpy(vendor_id, id, 3);
	id += 4;

	unsigned long id_hex = strtoul(id, NULL, 16);

	snprintf(dest, 8, "%.3s%04lX", vendor_id, id_hex);

	printk("::: converting to cnanonic: before: >%s<, after: >%s<\n", id, dest);
}

static int pnpof_get_resources(struct pnp_dev *dev)
{
	pnp_dbg(&dev->dev, "get resources\n");
	return pnpof_parse_allocated_resource(dev);
}

static int pnpof_set_resources(struct pnp_dev *dev)
{
	/* TODO: not too sure what this would do */

	return 0;
}

static int pnpof_disable_resources(struct pnp_dev *dev)
{
	/* TODO: not too sure what this would do */

	return 0;
}

struct pnp_protocol pnpof_protocol = {
	.name	 = "Plug and Play OF",
	.get	 = pnpof_get_resources,
	.set	 = pnpof_set_resources,
	.disable = pnpof_disable_resources,
};
EXPORT_SYMBOL(pnpof_protocol);

static int __init pnpof_get_id(char *pnpid, struct device_node *node)
{
	int i;
	int len;
	char *p;
	const __be32 *prop;

	prop = of_get_property(node, "compatible", &len);
	printk("::: prop is: %p\n", prop);
	i = 0;
	while (i < len) {
		p = (char *) prop + i;
		printk("::: p: >%s<\n", p);
		if(ispnpidof(p)) {
			printk("::: ispnpidof\n");
			pnpof_id_of_to_canonic(pnpid, p);
			return 0;
		} else {
			printk("::: is NOT pnpidof");
		}
		i += strlen(p) + 1;
	}

	return 1;
}

static int __init pnpof_add_node(struct device_node *node)
{
	struct pnp_dev *dev;
	char pnpid[8];
	char additional_pnpid[8];
	int error;

	int i;
	int len;
	char *p;
	const __be32 *prop; // FIXME order these

	/* TODO do we need to "Skip devices that are already bound", and how do we test for that */

	printk("::: evaluating a pnp node\n");

	error = pnpof_get_id(pnpid, node);
	if (error) {
		printk("::: %d\n", __LINE__);
		return 0;
	}

	printk("::: %d\n", __LINE__);

	dev = pnp_alloc_dev(&pnpof_protocol, num, pnpid);
	if (!dev) {
		printk("::: %d\n", __LINE__);
		return -ENOMEM;
	}

	printk("::: %d\n", __LINE__);
	set_primary_fwnode(&dev->dev, of_node_to_fwnode(node)); // TODO not entirely sure that this does what I think it does
	dev->data = node;

	dev->active = true; // FIXME - it wouldn't be detected if it had status=disabled, right?

	dev->capabilities |= PNP_READ;

	strncpy(dev->name, of_node_full_name(node), sizeof(of_node_full_name(node))); // TODO is this the right one

	if (dev->active)
		pnpof_parse_allocated_resource(dev); // FIXME FIXME now

	printk("::: %d\n", __LINE__);

	// XXX this seems to add all the pnp ids if there are multiple (which is presumably important)
	prop = of_get_property(node, "compatible", &len);
	i = 0;
	while (i < len) {
		p = (char *) prop + i;
		if(ispnpidof(p)) {
			printk("::: p is: >%s< and is pnpidof\n", p);
			pnpof_id_of_to_canonic(additional_pnpid, p);
			if(strcmp(additional_pnpid, pnpid)) // only add new ones, don't duplicate the main one
				pnp_add_id(dev, additional_pnpid);
		} else {
			printk("::: p is: >%s< and is NOT pnpidof\n", p);
		}
		i += strlen(p) + 1;
	}

	/* clear out the damaged flags */
	if (!dev->active)
		pnp_init_resources(dev);

	error = pnp_add_device(dev);
	if (error) {
		printk("::: %d\n", __LINE__);
		put_device(&dev->dev);
		return error;
	}

	num++;

	return 0;
}

int pnpof_disabled __initdata;
static int __init pnpof_init(void)
{
	struct device_node *node;

	if (pnpof_disabled) {
		printk(KERN_INFO "pnp: PnP OF: disabled\n");
		return 0;
	}
	printk(KERN_INFO "pnp: PnP OF init\n");
	pnp_register_protocol(&pnpof_protocol);

	for_each_compatible_node(node, NULL, "pnp") {
		pnpof_add_node(node);
	}

	printk(KERN_INFO "pnp: PnP OF: found %d devices\n", num);
	pnp_platform_devices = 1;
	return 0;
}

fs_initcall(pnpof_init);

static int __init pnpof_setup(char *str)
{
	if (str == NULL)
		return 1;
	if (!strncmp(str, "off", 3))
		pnpof_disabled = 1;
	return 1;
}

__setup("pnpof=", pnpof_setup);

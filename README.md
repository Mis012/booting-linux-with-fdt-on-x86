#### What is this?

This is my WIP tree for figuring out how best to fix and make viable booting with FDT 
(without ACPI) on x86 devices.

Included is a .dts (also WIP) for Asus FX505DY (which works "fine" with ACPI, at least 
with the asus-specific platform drivers, but I need to test this *somewhere*, and I really 
wanted to test the myth that "ACPI is the only way")

#### Can I try this out?

Making your own basic .dts should be trivial, mostly just delete the stuff that is different 
for you and it should *boot* ;)

stuff like PCI interrupts can be gotten from a running kernel, from ACPI tables, or in the best case 
scenario from hw documentation.

to compile your dts: `cpp -nostdinc -I ./include -undef -x assembler-with-cpp your.dts | ./scripts/dtc/dtc - -o your.dtb` (or just `dtc` if you're not using preprocessor features)

to boot with FDT, use the following command in grub (or the equivalent in e.g EFI Shell): `chainloader /bzImage dtb=/fx505dy.dtb acpi=off apic=debug pmtmr=0x808 root=<or use initramfs> rw`

!IMPORTANT: for FX505DY, `write_dword 0xFED815FC 0xFF001000` in grub (or equivalent in e.g. EFI shell) must be 
executed prior to launching Linux. This sets the GPIO controller's IRQ to edge triggered mode. 
(as opposed to level triggered mode, which causes issues for some yet-to-be-determined reason).

`apic=debug` is not required, but the output it gives is useful.

`pmtmr=0x808` is required, since Linux uses it to calibrate the timer it actually wants to use. 
the right value can be found in `/proc/ioports` (pmtmr is sort of tied to ACPI, so the fact that 
using it without ACPI is this simple is pretty nice :)

`root=X` is not needed if you go through the effort of building an initramfs

GPU works-ish if you build the firmware into the kernel (or if you build amdgpu as a module, aka 
the the right way to do it though it should really probe after root=X gets mounted if it wants 
the fw that bad)

Current state on fx505dy is that stuff mostly works, though dmesg gets spammed with APIC errors (not sure why).

One "small" issue is that it doesn't want to boot to X11 (gdm), resp. it does so just fine 
from time to time on random, all the other attempts result in amdgpu printing errors in dmesg and 
the display going black (it's not a freeze, just display doesn't work).

errors in question (log courtesy of `journalctl -k -b -1 --grep 'drm|amdgpu'`):
```
kernel: amdgpu 0000:05:00.0: amdgpu: failed to write reg 28b4 wait reg 28c6
kernel: amdgpu 0000:05:00.0: amdgpu: failed to write reg 1a6f4 wait reg 1a706
kernel: [drm:drm_mode_addfb2] [FB:93]
kernel: [drm:amdgpu_dm_atomic_commit_tail] *ERROR* Waiting for fences timed out!
kernel: [drm:dcn10_program_pipe] Un-gated front end for pipe 0
kernel: [drm:amdgpu_job_timedout] *ERROR* ring gfx timeout, signaled seq=4, emitted seq=6
kernel: [drm:amdgpu_job_timedout] *ERROR* Process information: process gnome-shell pid 826 thread gnome-shel:cs0 pid 862
kernel: amdgpu 0000:05:00.0: amdgpu: GPU reset begin!
kernel: [drm:drm_mode_addfb2] [FB:66]
kernel: [drm:amdgpu_cs_ioctl] *ERROR* Failed to initialize parser -125!
```


if you don't care about GUI and just want working fbcon, `nomodeset` should do the trick

if something goes wrong and you don't get far enough that fbcon pops up on efifb, 
you can use `earlycon=efifb keep_bootcon`, which will scroll kernel messages up your 
screen at a very slow rate, but from a much earlier point in the boot process, 
and should let you identify the issue. 
